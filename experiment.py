########
# IMPORTS INVOLVING RANDOMNESS TO SET SEEDS
import os
import random as rn

def set_seeds(seed=123):
    """
    Sets the seeds of all relevant libraries to get closer to reproducibility.
    This process depends on the frameworks you use, check their documentation.
    E.g. numpy.random.seed(seed) or tensorflow.random.set_seed(seed=seed)
    """
    # Import the libraries involving randomness in the experiment.py and
    # provide the code to set its seeds here.
    rn.seed(seed)

set_seeds(seed=os.environ['SEED'])

########
# OTHER IMPORTS
import time
from utils import get_config, get_data, get_model, train_model
from utils import log_func_src, log_config, log_keras_summary
from tensorflow.keras.callbacks import TensorBoard


########
# ALLOWING FOR MEMORY GROWTH ON A GPU IF DESIRABLE
# ...


########
# GET CONFIG
config = get_config()

########
# SET UP A DIRECTORY FOR STORING LOGS AND RESULTS
# Save the desired logs to this dir in the appropriate places in your code
EXP_ID = time.time()
LOG_DIR = os.path.join('results', str(EXP_ID))
os.makedirs(LOG_DIR)


########
# GET DATA
set_seeds(seed=config['SEED'])
(train_images, train_labels), (test_images, test_labels), class_names = get_data()


########
# GET MODEL
set_seeds(seed=config['SEED'])
model = get_model()

# Some logs
log_config(LOG_DIR, config, '{}_config.json'.format(EXP_ID))
log_func_src(LOG_DIR, get_model, '{}_model.py'.format(EXP_ID))
log_keras_summary(LOG_DIR, model, '{}_summary.txt'.format(EXP_ID))

########
# TRAIN THE MODEL
callbacks = [TensorBoard(log_dir=LOG_DIR)]  # Simpole log into tensorboard
set_seeds(seed=config['SEED'])
model, log = train_model(model, train_images, train_labels,
             epochs=config['EPOCHS'], batch_size=config['BATCHSIZE'],
             validation_data=(test_images, test_labels), callbacks=callbacks)


########
# RENAMING THE LOG_DIR TO DESIRED FORMAT
NEW_LOG_DIR = os.path.join('results', 'finished_exp_{}'.format(EXP_ID))
os.rename(src=LOG_DIR, dst=NEW_LOG_DIR)


print('Experiment {} finished successfully!'.format(EXP_ID))
