# Cifar10-CNN

Convolutional neural network implemented in tensorflow.keras trained on Cifar-10 dataset.
The project is a simple example of a repeatable ML experiment built using [repex_template](https://gitlab.com/paloha/repex-template).

Created as a teaching material for [Machine Learning in Physics Winter School 2020](https://www.esi.ac.at/events/e302/) organized by the [Vienna Doctoral School in Physics](https://vds-physics.univie.ac.at/) & [ESI](www.esi.ac.at).
