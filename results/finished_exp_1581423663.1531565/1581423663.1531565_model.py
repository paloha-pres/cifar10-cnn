def get_model(lr=0.001):
    """
    Definition of the model. Returns compiled model.
    The whole source code of this function can be saved.
    """
    from tensorflow.keras import models, layers
    from tensorflow.keras.losses import SparseCategoricalCrossentropy
    from tensorflow.keras.optimizers import Adam
    model = models.Sequential()
    model.add(layers.Conv2D(8, (3, 3), activation='relu', name='Conv_0', input_shape=(32, 32, 3)))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(16, (3, 3), activation='relu', name='Conv_1'))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(16, (3, 3), activation='relu', name='Conv_2'))
    model.add(layers.Flatten())
    model.add(layers.Dense(64, activation='relu', name='Dense_0'))
    model.add(layers.Dense(10, name='Dense_1'))

    optimizer = Adam(learning_rate=lr)
    model.compile(optimizer=optimizer, metrics=['accuracy'],
              loss=SparseCategoricalCrossentropy(from_logits=True))
    return model

