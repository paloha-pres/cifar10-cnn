import os, json

def get_config():
    """
    Function which reads and returns a config for the experiment.
    Either a ENV_VARIABLE parser or ARG_PARSER or file reader which
    returns a dictionary {'config_key': value}
    """
    import os
    config = {
        'SEED': int(os.environ.get('SEED')),
        'BATCHSIZE': int(os.environ.get('BATCHSIZE', 32)),
        'LR': float(os.environ.get('LR', 0.001)),
        'EPOCHS': int(os.environ.get('EPOCHS', 2)),
    }
    return config


def get_data():
    """
    Data loader function. Can be a synthetic data generator, generator
    class like keras.Sequence or just a function that loads the data from
    a file on a drive or from a np.memmap etc.
    Here we load CIFAR-10 data.
    """
    from tensorflow.keras import datasets
    (train_images, train_labels), (test_images, test_labels) = datasets.cifar10.load_data()
    # Normalize pixel values to be between 0 and 1
    train_images, test_images = train_images / 255.0, test_images / 255.0
    class_names = ['airplane', 'automobile', 'bird', 'cat', 'deer',
                   'dog', 'frog', 'horse', 'ship', 'truck']

    return (train_images, train_labels), (test_images, test_labels), class_names


def get_model(lr=0.001):
    """
    Definition of the model. Returns compiled model.
    The whole source code of this function can be saved.
    """
    from tensorflow.keras import models, layers
    from tensorflow.keras.losses import SparseCategoricalCrossentropy
    from tensorflow.keras.optimizers import Adam
    model = models.Sequential()
    model.add(layers.Conv2D(8, (3, 3), activation='relu', name='Conv_0', input_shape=(32, 32, 3)))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(16, (3, 3), activation='relu', name='Conv_1'))
    model.add(layers.MaxPooling2D((2, 2)))
    model.add(layers.Conv2D(16, (3, 3), activation='relu', name='Conv_2'))
    model.add(layers.Flatten())
    model.add(layers.Dense(64, activation='relu', name='Dense_0'))
    model.add(layers.Dense(10, name='Dense_1'))

    optimizer = Adam(learning_rate=lr)
    model.compile(optimizer=optimizer, metrics=['accuracy'],
              loss=SparseCategoricalCrossentropy(from_logits=True))
    return model


def train_model(model, train_images, train_labels, epochs=2,
                batch_size=32, validation_data=None, callbacks=None):
    """
    Returns a trained model train history.
    """
    history = model.fit(train_images, train_labels, epochs=epochs,
                        validation_data=validation_data, batch_size=batch_size,
                        callbacks=callbacks)
    return model, history


########
# LOGGING helper functions


def log_config(log_dir, config, fname='config.json'):
    """
    Save experiment config to json file
    """
    with open(os.path.join(log_dir, fname), 'w', encoding='utf-8') as f:
        json.dump(config, f, ensure_ascii=False, indent=4)


def log_func_src(log_dir, func, fname=None):
    """
    Save source code of a function to a file
    """
    import inspect
    fname = '{}.py'.format(func.__name__) if fname is None else fname
    path = os.path.join(log_dir, fname)
    source = inspect.getsource(func)
    with open(path, 'w') as outfile:
        print(source, file=outfile)


def log_keras_summary(log_dir, model, fname='summary.txt', mode='a'):
    """
    Save model summary
    """
    def myprint(s):
        with open(os.path.join(log_dir, fname), mode) as f:
            print(s, file=f)
    model.summary(print_fn=myprint)
